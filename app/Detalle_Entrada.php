<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Entrada  extends Model
{
	
	/**
	*
	*@var array
	*/
	protected $fillable = [
    'idEntradas','idProductos','CantidadEntradas' 
];

public $timestamps = false;
public $table = "detalle_entrada";
}