<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entradas  extends Model
{
	/**
	*
	*@var array
	*/
	protected $fillable = [
    'FechaEntradas','TotalEntradas','Estado',
];
}