<?php

namespace App\Http\Controllers;

use App\Categorias;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $Categoria = Categorias::all();
        $datos['Categorias'] = Categorias::paginate(5);
        return view('Categorias.index', $datos);

    }

    public function create()
    {
        return view('Categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombreCategoria' => 'required|max:40',
            'descripcionCategoria' => 'max:300'
        ]);

        $campos = [
            'nombreCategoria' => 'required|string|max:40',

        ];

        $mensaje = ["required" => 'El :attribute es requerido'];

        $this->validate($request, $campos, $mensaje);



        $datosCategoriaas = request()->except('_token');

        Categorias::insert($datosCategoriaas); // aca insertaamos esta informacion a la base de datos

        /* return response()->json($datosCliente); */

        return redirect('Categorias');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categorias  $Categorias
     * @return \Illuminate\Http\Response
     */
    public function edit($categoria)
    {
        return view('Categorias.edit')->with(['Categorias' => Categorias::findOrFail($categoria)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categorias  $Categorias
     * @return \Illuminate\Http\Response
     */
    public function update($categoria)
    {
        request()->validate([
            'nombreCategoria' => 'required|max:40',
            'descripcionCategoria' => 'required|max:300'
        ]);

        $categorias = Categorias::findOrFail($categoria);
        $categorias->Update(request()->all());
        return redirect()->route('Categorias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categorias  $Categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorias = Categorias::findOrFail($id);
        $categorias->delete();
        return redirect()->route('Categorias.index');
    }
}
