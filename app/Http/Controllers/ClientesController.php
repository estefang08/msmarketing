<?php

namespace App\Http\Controllers;

use App\Clientes;

use Illuminate\Http\Request;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
            if($request){
                $query = trim ($request->get('search'));

                $datos = Clientes::where('nombre', 'LIKE', '%' . $query . '%' )
                ->orderBy('id','asc')
                ->paginate(5);

                return view('clientes.index', ['clientes' => $datos, 'search' => $query]);
                
            }
 


/* 
        $datos['clientes'] = Clientes::paginate(5);
        return view('clientes.index', $datos); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $datosCliente=request()->all(); */

        $campos=[
            'Tipo_identificacion' => 'required|string|max:100',
            'id'=> 'required|string|max:10|min:6',
            'nombre' => 'required|string|max:100',
            'apellido' => 'required|string|max:100',
            'edad' => 'required|string|max:2|min:2',
            'celular' => 'required|string|max:10|min:10',
        ];

        $mensaje= ["required" => 'El :attribute es requerido'];

            $this->validate($request,$campos,$mensaje);


        $datosCliente = request()->except('_token');

        Clientes::insert($datosCliente); // aca insertaamos esta informacion a la base de datos 

        /* return response()->json($datosCliente); */

        return redirect('/clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $cliente = Clientes:: findOrFail($id);

        return view('clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $campos=[
            'Tipo_identificacion' => 'required|string|max:100',
            'id'=> 'required|string|max:10|min:10',
            'nombre' => 'required|string|max:100',
            'apellido' => 'required|string|max:100',
            'edad' => 'required|string|max:2|min:2',
            'celular' => 'required|string|max:10|min:10',
        ];

        $mensaje= ["required" => 'El :attribute es requerido'];

            $this->validate($request,$campos,$mensaje); 
        $datosCliente = request()->except(['_token','_method']);
        Clientes::where('id','=',$id)->update($datosCliente);
        
        $cliente = Clientes:: findOrFail($id);

        return redirect('clientes');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = clientes::findOrFail($id);
        $cliente->delete();

        return redirect('/clientes');
    }
}