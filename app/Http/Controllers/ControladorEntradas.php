<?php



namespace App\Http\Controllers;

use App\Entradas;
use App\Productos;
use App\Detalle_Entrada;
use Alert;
use DB;
use Illuminate\Http\Request;
use PDF;


class ControladorEntradas extends Controller
{
    public function index(){
    	return view('Entradas.index')->with(['Entradas'=>Entradas::paginate(6)]);
    }

    public function create(){
        $productos = Productos::join("proveedores","proveedores.id","=","Productos.idProveedor")->select("Productos.id","proveedores.NombreProveedor","Productos.NombreProducto","Productos.CantidadProducto","Productos.CantidadLimite","Productos.PrecioUnitario")->paginate(4);
    	return view('Entradas.crear',compact("productos"));
    }

    public function store(Request $request){
        alert()->success('Registro exitoso', 'Entrada creada!')->persistent("Cerrar");
        $Valor = $request->all();
        try {
            //BD:beginTransaction();
            $entrada = Entradas::create([
                "FechaEntradas"=> $Valor["FechaEntradas"],
                "TotalEntradas"=> $Valor["TotalEntradas"],
             ]);
            foreach ($Valor["id"] as $key => $value) {
                Detalle_Entrada::create([
                    "idEntradas"=> $entrada->id,
                    "idProductos"=> $value,
                    "CantidadEntradas"=> $Valor["Cantidad"][$key]
                ]);
            }
        } catch (Exception $e) {

        }
    	return redirect()
    		->route('entradas.index')->with('success', 'entrada registrada!');
    }

    public function edit($entrada){
    	return view('Entradas.editar')->with(['entrada'=>Entradas::findOrFail($entrada),]);
    }

    public function detail($entrada){

        $id = $entrada;
        $entradas = [];
        $entradas = Detalle_Entrada::paginate(5);
        $entradas = Detalle_Entrada::select("detalle_entrada.*", "detalle_entrada.CantidadEntradas as cantidad", "detalle_entrada.idProductos as productos","detalle_entrada.idEntradas as identradas")
        ->join("entradas", "Detalle_Entrada.idEntradas", "=", "entradas.id")
        ->where("entradas.id", $id)
        ->paginate(5);

        return view("Entradas.detail", compact("entradas"));
    }

    public function update($entrada){
        alert()->success('Registro exitoso','Entrada registrada',)->persistent("Cerrar");
    	$entrada = Entradas::findOrFail($entrada); //Buscar datos del entrada a modificar
    	$entrada -> update(request()->all()); //Actualizar el entrada
    	return redirect()
    		->route('entradas.index');
    }

    public function verpdf()
    {
        $productos = Entradas::orderBy('id','asc')->get();
        // productosPdf is the view that includes the downloading content
        $view = \View::make('entradas.verpdf', ['entradas'=>$Entradas]);
        $html_content = $view->render();
        // Set title in the PDF
        PDF::SetTitle("Lista de Entradas");
        PDF::SetFont('helvetica', 'B', 8);
        PDF::SetCreator(PDF_CREATOR);
        PDF::SetAuthor('Nicola Asuni');
        PDF::SetTitle('TCPDF Example 048');
        PDF::SetSubject('TCPDF Tutorial');
        PDF::SetKeywords('TCPDF, PDF, example, test, guide');
        PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);
        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');
        // userlist is the name of the PDF downloading
        PDF::Output('lista_productos.pdf');
    }
    public function destroy($entradaid){
        alert()->success('Cambio de estado Exitoso', 'estado modificado!')->persistent("Cerrar");
        $nuevoestado = 0;
        //$entrada = entrada::findOrFail($entrada)->makeHidden([ 'id','Nombreentrada','Telefono','Correo','created_at','updated_at']); //Buscar datos del entrada a modificar
        list($estado) = DB::table('entradas')->select('estado')->where('id','=',$entradaid)->get('estado');
        //$entradaEstado = $entrada->fetchAll();
        if ($estado->estado == 1) {
           $nuevoestado = 2;
        }
        else{
            $nuevoestado = 1;
        }
        $entrada =DB::table('entradas')->where('id',$entradaid)->update(['estado'=>$nuevoestado]); //Actualizar el entrada
        return redirect()
            ->route('entradas.index');
    }
}
