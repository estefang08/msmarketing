<?php



namespace App\Http\Controllers;

use App\Productos;
use App\Categorias;
use Illuminate\Http\Request;
use Alert;
use App\Proveedor;
use PDF;
use DB;

class ControladorProductos extends Controller
{
    public function index(){
        //$productos = DB::table('productos')
          //  ->join('proveedores', 'productos.idProveedor', '=', 'proveedores.id')
            //->select('productos.id', 'productos.idCategoria', 'proveedores.NombreProveedor', 'productos.NombreProducto', 'productos.CantidadProducto', 'productos.CantidadLimite', 'productos.PrecioUnitario', 'productos.Estado')
            //->paginate(5);
        $productos = DB::table('productos')
            ->join('proveedores', 'productos.idProveedor', '=', 'proveedores.id')
            ->select('*', 'productos.id','proveedores.NombreProveedor', 'productos.Estado')
            ->paginate(10);
        return view('productos.index')->with(['productos'=>$productos]);
    }

    public function create(){
        $categoria = Categorias::all();
        $proveedores = Proveedor::all();
        $proveedore=DB::table('proveedores')
            ->select('*')
            ->where('Estado', '=', '1s')
            ->get();
    	return view('productos.crear', compact('categoria','proveedores'));
    }

    public function store(){
        $Categoria = Categorias::all();
        alert()->success('Registro exitoso', 'Producto creado!')->persistent("Cerrar");
    	$producto = Productos::create(request()->all());
    	return redirect()
    		->route('productos.index');
    }

    public function edit($producto){
    	return view('productos.editar')->with(['producto'=>Productos::findOrFail($producto),]);
    }

    public function update($producto){
        alert()->success('Registro exitoso', 'Producto editado!')->persistent("Cerrar");
    	$producto = Productos::findOrFail($producto); //Buscar datos del producto a modificar
    	$producto -> update(request()->all()); //Actualizar el producto
    	return redirect()
    		->route('productos.index');
    }

    public function verpdf()
    {
        $productos = Productos::orderBy('id','asc')->get();
        // productosPdf is the view that includes the downloading content
        $view = \View::make('productos.verpdf', ['productos'=>$productos]);
        $html_content = $view->render();
        // Set title in the PDF
        PDF::SetTitle("Lista de productos");
        PDF::SetFont('helvetica', 'B', 8);
        PDF::SetCreator(PDF_CREATOR);
        PDF::SetAuthor('Nicola Asuni');
        PDF::SetTitle('TCPDF Example 048');
        PDF::SetSubject('TCPDF Tutorial');
        PDF::SetKeywords('TCPDF, PDF, example, test, guide');
        PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);
        // set header and footer fonts
        PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');
        // userlist is the name of the PDF downloading
        PDF::Output('lista_productos.pdf');
    }

     public function destroy($productoid){
        alert()->success('Cambio de estado Exitoso', 'estado modificado!')->persistent("Cerrar");
        $nuevoestado = 0;
        //$producto = producto::findOrFail($producto)->makeHidden([ 'id','Nombreproducto','Telefono','Correo','created_at','updated_at']); //Buscar datos del producto a modificar
        list($estado) = DB::table('productos')->select('estado')->where('id','=',$productoid)->get('estado');
        //$productoEstado = $producto->fetchAll();
        if ($estado->estado == 1) {
           $nuevoestado = 2;
        }
        else{
            $nuevoestado = 1;
        }
        $producto =DB::table('productos')->where('id',$productoid)->update(['estado'=>$nuevoestado]); //Actualizar el producto
        return redirect()
            ->route('productos.index');
    }
}


