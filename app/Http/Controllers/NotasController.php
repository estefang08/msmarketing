<?php

namespace App\Http\Controllers;

use App\notas;
use Illuminate\Http\Request;

class NotasController extends Controller
{
    public function index()
    {

        return view('notas.todas.index', ['notas' => notas::all()]);
    }

    public function store(Request $request)
    {

        $nota = new notas();
        $nota->titulo = request('titulo');
        $nota->texto = request('texto');
        $nota->user_id = auth()->id();

        $nota->save();

        return redirect('notas/todas');
    }

    public function favoritas()
    {

        return view('notas.favoritas', ['notas' => notas::all()]);
    }

    public function archivadas()
    {

        return view('notas.archivadas', ['notas' => notas::all()]);
    }
}
