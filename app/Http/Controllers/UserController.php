<?php

namespace App\Http\Controllers;


use Illuminate\Database\QueryException;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\UserEditFormRequest;
use App\User;
use Illuminate\Http\Request;
use Prophecy\Promise\ReturnPromise;
use App\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request) {
            $query = trim($request->get('search'));

            $users = User::where('name', 'LIKE', '%' . $query . '%')
                ->orderBy('id', 'asc')
                ->paginate(5);


            return view('usuarios.index', ['users' => $users, 'search'
            => $query]);
        }


        /*   $users = User::all();
        return view('usuarios.index', ['users' => $users]); */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('usuarios.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request) //aca guarmos los datos a la base de datos 

    {
        $usuario = new User();

        $usuario->name = request('name');
        $usuario->email = request('email');
        $usuario->password = bcrypt(request('password'));

        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/imagenes', $file->getClientOriginalName());
            $usuario->imagen = $file->getClientOriginalName();
        }

        $usuario->save();

        $usuario->asignarRol($request->get('rol')); // aca asiganamos el rol que biene desde el formulario

        return redirect('/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Userr  $userr
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('usuarios.show', ['user' => User::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Userr  $userr
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view('usuarios.edit', ['user' => $user, 'roles'  => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Userr  $userr
     * @return \Illuminate\Http\Response
     */

    public function update(UserEditFormRequest $request, $id)
    {

        $this->validate(request(), ['email' => ['required', 'email', 'max:225', 'unique:users,email,' . $id]]);
        $this->validate(request(),    ['name'=> ['max:10','min:6']]);
        $usuario =  User::findOrFail($id);

        $usuario->name = $request->get('name');
        $usuario->email = $request->get('email');

        if ($request->hasFile('imagen')) {
            $file = $request->imagen;
            $file->move(public_path() . '/imagenes', $file->getClientOriginalName());
            $usuario->imagen = $file->getClientOriginalName();
        }

        $pass = $request->get('password');
        if ($pass !=  null) {
            $usuario->password = bcrypt($request->get('password'));
        } else {  
            unset($usuario->password);
        }

        $role = $usuario->roles;
        if(count($role)>0){
            $role_id =$role[0]->id;
        }

        User::find($id)->roles()->updateExistingPivot($role_id,[ 'role_id' => $request->get('rol') ]);


        $usuario->update();

        return redirect('/usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Userr  $userr
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = user::findOrFail($id);
        $usuario->delete();

        return redirect('/usuarios');
    }
}