<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
	/**
	*
	*@var array
	*/
	protected $fillable = [
    'IdCategoria', 'IdProveedor','NombreProducto', 'CantidadProducto', 'CantidadLimite','PrecioCompra','PrecioUnitario','Estado',  
];
}

