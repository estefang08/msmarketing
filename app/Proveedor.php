<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    /**
	*
	*@var array
	*/
	protected $fillable = [
    'NombreProveedor','Telefono','Correo','Estado',
];

public $table = "proveedores";
}
