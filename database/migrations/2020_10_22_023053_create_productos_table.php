<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idCategoria')->nullable();
            $table->BigInteger('idProveedor')->unsigned();
            $table->string('NombreProducto',50);
            $table->integer('CantidadProducto')->nullable();
            $table->integer('CantidadLimite');
            $table->integer('PrecioCompra')->nullable();
            $table->integer('PrecioUnitario');
            $table->char('Estado',2);
            $table->timestamps();
            $table->foreign('idProveedor')->references('id')->on('proveedores');
            $table->foreign('idCategoria')->references('id')->on('categorias')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
