<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Crear - Categoria</title>

</head>

<body>
    @extends('layouts.app')

    @section('content')
    <form action="{{url ('/Categorias')}}" method="post">
        {{csrf_field()}}
        <div class="container">
            <div class="row">




                <div class="form-group col-md-6">
                    <label>Nombre de la categoría</label>
                    <input type="text" class="form-control {{$errors->has('nombreCategoria')?'is-invalid':''}}" name="nombreCategoria" placeholder="Nombre Categoria">
                    {!! $errors->first('nombreCategoria', '<div class="invalid-feedback">El campo nombre categoria es obligatorio</div>')!!}

                </div>

            </div>


            <div class="row">

                <div class="form-group col-md-6">
                    <label>Descripción de la categoría</label>
                    <textarea name="descripcionCategoria" id="descripcionCategoria" class="form-control" cols="30" rows="10" placeholder="Descripción Categoria"></textarea>


                </div>

            </div>


           <div class="row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary">Registrar categoría</button>
                    <button type="reset" class="btn btn-danger">Borrar Campos</button>
                    <a href="{{url('Categorias')}}" class="btn btn-primary">Regresar</a>
                </div>
            </div>
        </div>
    </form>

    @endsection
</body>

</html>
