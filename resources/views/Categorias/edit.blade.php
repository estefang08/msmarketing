<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Modificar - Categoria</title>

</head>

<body>
    @extends('layouts.app')

    @section('content')


    <form action="{{ route('Categorias.update', $Categorias->id)}}" method="POST">
        {{csrf_field()}}
        @method('PATCH')
        <div class="container">

            <div class="row">




                <div class="form-group col-md-6">
                    <label>Nombre de la categoría</label>
                    <input type="text" value="{{$Categorias->nombreCategoria}}" class="form-control {{$errors->has('nombreCategoria')?'is-invalid':''}}" name="nombreCategoria" placeholder="Nombre Categoria">
                    {!! $errors->first('nombreCategoria', '<div class="invalid-feedback">El campo nombre categoria es obligatorio</div>')!!}

                </div>

            </div>


            <div class="row">

                <div class="form-group col-md-6">
                    <label>Descripción de la categoría</label>
                    <textarea name="descripcionCategoria"  id="descripcionCategoria" class="form-control {{$errors->has('descripcionCategoria')?'is-invalid':''}}" cols="30" rows="10" placeholder="Descripcion Categoria">{{$Categorias->descripcionCategoria}}</textarea>
                    {!! $errors->first('descripcionCategoria', '<div class="invalid-feedback">El campo descripcion de categoria es obligatorio</div>')!!}

                </div>

            </div>


            <div class="row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary">Modificar categoría</button>
                    <button type="reset" class="btn btn-danger">Borrar Campos</button>
                    <a href="{{url('Categorias')}}" class="btn btn-primary">Regresar</a>
                </div>
            </div>
        </div>
    </form>

    @endsection
</body>

</html>
