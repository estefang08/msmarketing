<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Categorías</title>

</head>

<body>
@extends('layouts.app')

@section('content')
<div class="container">
    <h2><b> Lista de categorías. </b><a href="Categorias/create"><button type="button" class="btn btn-success float-right ">Registrar categoría</button></a></h2>


    <table class="table table-light table-hover">
        <br><br>
        <thead>
            <tr>
                <th scope="col">Nombre de la Categoría</th>
                <th scope="col">Descripción de la Categoría</th>
                <th scope="col">Opciones</th>

            </tr>
        </thead>

        <tbody>
            @foreach ($Categorias as $categoria)
            <tr>
                <td>{{$categoria->nombreCategoria}}</td>
                <td>{{$categoria->descripcionCategoria}}</td>
                <td>

                    <form action="{{route('Categorias.destroy',$categoria->id)}}" method="POST">
                        <a href="{{route('Categorias.edit', $categoria->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button></a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-exchange-alt"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
        </table>
        <div class="d-flex justify-content-center">
        {{ $Categorias->links() }}
</div>
</div>




@endsection
</body>
</html>
