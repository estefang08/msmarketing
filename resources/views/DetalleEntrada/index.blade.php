	@extends('layouts.app')   
@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Listado Detalle de la Entrada</title>
</head>
<body>

	<h1 align="center">Listado Detalle Entradas</h1>
	<a class="btn btn-success mb-3" href="{{ route('DetalleEntrada.create') }}">Crear</a>
	@if(empty($Detalle_Entrada))
		<div class="alert alert-warning">
			La lista de docuementos esta vacia
		</div>
	@else
		<div class="class-resposive">
			<table class="table table-striped">
				<thead class="thead-light">
					<tr>
						<th>Id Entradas</th>
						<th>Id Productos</th>
						<th>Cantidad de Entradas</th>
						
						
						
					</tr>
				</thead>
				<tbody>
					@foreach($Detalle_Entrada as $DetalleEntrada)
					<tr>
						<td>{{ $Detalle_Entrada->idEntradas}}</td>
						<td>{{ $Detalle_Entrada->idProductos}}</td>
						<td>{{ $Detalle_Entrada->CantidadEntradas}}</td>
						
						
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endif
</body>
</html>
@endsection