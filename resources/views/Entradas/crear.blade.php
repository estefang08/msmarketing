<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Crear entrada</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">

	<h2>Crear Entradas<a class="btn btn-primary float-right mb-2" style="margin:3px;" href="{{ route('entradas.index') }}">Volver</a></h2>
	<form action="{{ route('Entradas.store') }}" method="POST">
		@csrf

		<div class="row">
			<div class="col-12">
			<div class="card">
				<div class="row card-body">
			<div class="form-group col-4">
			<label>Fecha de entradas </label>
			<input class="form-control" onmouseleave="validarFecha()" type="date" name="FechaEntradas" id="FechaEntradas" value="{{ old('FechaEntradas') }}" required>
			</div>

		<div class="form-group col-6">
			<label>Total entradas </label>
			<input class="form-control" id="precio_total" readonly="" type="number" name="TotalEntradas" value="{{ old('TotalEntradas') }}" required>
		</div>


		<div class="form-group col-10">
			<div class="class-resposive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Id Producto</th>
						<th>Nombre</th>
						<th>Cantidad</th>
						<th>Precio C/U</th>
						<th>Subtotal</th>
						<th>Acción</th>
					</tr>
				</thead>

				<tbody id="tblProductos">

				</tbody>
			</table>
			</div>
		</div>
		<div class="form-group col-6">
            <div class="form-row">
                <input class="form-control" type="hidden" name="Estado" id="Estado" value="1">
            </div>
			<button type="submit" class="btn btn-success btn-lg mt-3">Añadir nuevo ingreso</button>
		</div>


		</div>
		</div>
		</div>
		<div class="col-12">
		<div class="card">
			<div class="row card-body">
		@if(empty($productos))
		<div class="alert alert-warning">
			La lista de productos está vacía
		</div>
	@else
	<div class="form-group">
		<div class="class-resposive">
			<table id="TablaProductos" class="table table-striped ">
				<thead>
					<tr>
						<th>Id Productos</th>
						<th>Nombre Proveedor</th>
						<th>Nombre del Producto</th>
						<th>Stock</th>
						<th>Cantidad del Producto</th>
						<th>Precio de Compra</th>
						<th>Precio Unitario</th>
						<th>Acciones</th>

					</tr>



				</thead>
				<tbody>
					@foreach($productos as $producto)
					<tr id="tr-original{{ $producto->id }}">
						<td>{{ $producto->id }}</td>
						<td>{{ $producto->NombreProveedor }}</td>
						<td>{{ $producto->NombreProducto}}</td>
						<td>{{ $producto->CantidadProducto}}</td>
						<td><input class="form-control" min="0" max="10" type="number" id="CantidadProducto{{ $producto->id }}" ></td>
						<td><input class="form-control" type="number" id="PrecioCompra{{ $producto->id }}" ></td>
						<td><input class="form-control" type="number" id="PrecioUnitario{{ $producto->id }}" ></td>
						<td><button id="boton_apagado{{ $producto->id }}" onclick="agregar_producto('{{ $producto->id }}','{{ $producto->NombreProducto}}','{{ $producto->CantidadLimite}}')" type="button"
                            class="btn btn-success float-right">Agregar</button>

						</td>




					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		</div>
		</div>

    @endif
    <div class="d-flex justify-content-center">
        {!! $productos->links() !!}
    </div>
	</form>


</div>
		</div>



@endsection
</body>
</html>
@section("script")
<script>
function validarFecha(){
	var hoy             = new Date();
	var fechaFormulario = document.getElementById("FechaEntradas").value;
	hoy = hoy.getFullYear() + "-" + (hoy.getMonth() +1) + "-" + hoy.getDate();
	// Comparamos solo las fechas => no las horas!!

	if (hoy < fechaFormulario) {
		swal({
                title: 'La fecha debe ser menor o igual a la actual',
                type: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!'
            })
		document.getElementById('FechaEntradas').value = hoy;
	}
}
function agregar_producto(id,Nombre,Stock) {
        var Cantidad =  document.getElementById("CantidadProducto"+id).value;



	if (parseInt(Cantidad) >= 1){

		document.getElementById("boton_apagado"+id).disabled = true;
        document.getElementById("CantidadProducto"+id).disabled = true;
        document.getElementById("PrecioCompra"+id).disabled = true;
		document.getElementById("PrecioUnitario"+id).disabled = true;
		var Precio = document.getElementById("PrecioUnitario"+id).value;

	 $("#tblProductos").append(`
                    <tr id="tr-${id}">
                    	<td>
                    		<input type="hidden" name="id[]" value="${id}" />
                    		<input type="hidden" name="Cantidad[]" value="${Cantidad}" />
                   			${id}
                   		</td>
                        <td>${Nombre}</td>
                        <td>${Cantidad}</td>
                        <td>${Precio}</td>
                        <td>${parseInt(Cantidad) * parseInt(Precio)}</td>
                        <td>
                            <button type="button" class="btn btn-danger" onclick="eliminar_producto(${id}, ${parseInt(Cantidad) * parseInt(Precio)})">X</button>
                        </td>

                    </tr>
                `);
	 let precio_total = $("#precio_total").val() || 0;
            $("#precio_total").val(parseInt(precio_total) + parseInt(Cantidad) * parseInt(Precio));
    } else {
    	swal({
                title: 'No se puede agregar la entrada de un producto si esta es menor a 0',
                type: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!'
            })
    }
}

function eliminar_producto(id, subtotal) {
        $("#tr-" + id).remove();

        let precio_total = $("#precio_total").val() || 0;
        $("#precio_total").val(parseInt(precio_total) - subtotal);
        document.getElementById("boton_apagado"+id).disabled = false;
        document.getElementById("CantidadProducto"+id).disabled = false;
        document.getElementById("PrecioCompra"+id).disabled = false;
		document.getElementById("PrecioUnitario"+id).disabled = false;
    }

    $(document).ready(function() {
    $("#CantidadProducto").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

    $(document).ready(function() {
    $("#CantidadProducto").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
</script>
@endsection
