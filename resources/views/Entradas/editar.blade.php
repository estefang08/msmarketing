<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Modificar entrada</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h2> Entradas<a class="btn btn-primary float-right mb-2" style="margin:3px;" href="{{ route('entradas.index') }}">Volver</a></h2>
	<form action="{{ route('Entradas.update', ['entrada' => $entrada->id]) }}" method="POST">
		@csrf
		@method('PUT')
		<br>
		<div class="form-row">
			<label>Fecha Entrada</label>
			<input class="form-control" type="date" name="FechaEntradas" value="{{ $entrada->FechaEntradas }}" required>
		</div>

		<div class="form-row">
			<label>Total Entrada</label>
			<input class="form-control" type="number" name="TotalEntradas" value="{{ $entrada->TotalEntradas }}" required>
		</div>


		<div class="form-row">
			<button type="submit" class="btn btn-success btn-lg mt-3">Editar entrada</button>
		</div>


	</form>

@endsection
</body>
</html>
