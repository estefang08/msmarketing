<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Msmarketing | Crear producto</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h3>Crear Producto<a class="btn btn-primary float-right mb-3" style="margin:3px;" href="{{ route('productos.index') }}">Volver</a></h3>
	<form action="{{ route('productos.store') }}" method="POST">
		@csrf
		<div class="form-row">
			<label>Nombre del producto</label>
			<input class="form-control" type="text" name="NombreProducto" id="NombreProducto" value="{{ old('NombreProducto') }}" required>
		</div>
		<div class="form-row">
			<label>Cantidad del producto</label>
			<input class="form-control" maxlength="5" type="text" name="CantidadProducto" id="CantidadProducto" value="{{ old('CantidadProducto') }}" >
		</div>
		<div class="form-row">
			<label>Cantidad Límite</label>
			<input class="form-control" maxlength="5" type="text" name="CantidadLimite" id="CantidadLimite" value="{{ old('CantidadLimite') }}" required>
		</div>
		<div class="form-row">
			<label>Precio Unitario</label>
			<input class="form-control" maxlength="5" type="text" name="PrecioUnitario" id="PrecioUnitario" min="1.0" step="1" value="{{ old('PrecioUnitario') }}" required>
		</div>

		<br>
		<div class="form-row">
		<div class="form-group col-md-4" >
			<label>Proveedor</label>
			<select id="Estado" class="form-control" name="IdProveedor"  value="{{ old('IdProveedor') }}" required>
				<option value="">Seleccione</option>
				@foreach($proveedores as $Proveedor)
					<option value="{{ $Proveedor->id }}">{{$Proveedor->NombreProveedor}}</option>
				@endforeach
			</select>
		</div>
		</div>
		<div class="form-row">
		<div class="form-group col-md-4">
			<label>Categoría</label>
			<select id="Estado"  class="form-control" name="IdCategoria"  value="{{ old('IdCategoria') }}">
				<option value="">Seleccione</option>
				@foreach($categoria as $categorias)
					<option value="{{ $categorias->id }}">{{$categorias->nombreCategoria}}</option>
				@endforeach
			</select>
		</div>
		</div>
		<div class="form-row">
			<input class="form-control" type="hidden" name="Estado" id="Estado" value="1">
		</div>

		<div class="form-row">
			<button type="submit" class="btn btn-success btn-lg mt-3">Crear producto</button>
		</div>




	</form>

@endsection
</body>
</html>
@section("script")
<script>

	$(document).ready(function() {
    $("#CantidadProducto").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
	$(document).ready(function() {
    $("#CantidadLimite").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
	$(document).ready(function() {
    $("#PrecioUnitario").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
	</script>
@endsection
