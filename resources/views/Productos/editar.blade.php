<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Msmarketing | Modificar producto</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h2>Editar Producto<a class="btn btn-success float-right mb-3" style="margin:3px;" href="{{ route('productos.index') }}">Volver</a></h2>
	<form action="{{ route('productos.update', ['producto' => $producto->id]) }}" method="POST">
		@csrf
		@method('PUT')
		<br>
		<div class="form-row">
			<label>Nombre del producto</label>
			<input class="form-control" type="text" name="NombreProducto" value="{{ $producto->NombreProducto }}" required>
		</div>
		<div class="form-row">
			<label>Cantidad de producto</label>
			<input class="form-control" type="number" name="CantidadProducto" value="{{ $producto->CantidadProducto }}" required>
		</div>
		<div class="form-row">
			<label>Cantidad Límite</label>
			<input class="form-control" type="number" name="CantidadLimite" value="{{ $producto->CantidadLimite }}" required>
		</div>
		<div class="form-row">
			<label>Precio Unitario</label>
			<input class="form-control" type="number" name="PrecioUnitario" min="1.0" step="1" value="{{ $producto->PrecioUnitario }}" required>
		</div>
		<div class="form-row">
			<input class="form-control" type="hidden" name="Estado" id="Estado" value="{{ $producto->Estado }}">
		</div>

		<div class="form-row">
			<button type="submit" class="btn btn-primary btn-lg mt-3">Editar producto</button>
		</div>



	</form>

@endsection
</body>
</html>
