<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Msmarketing | Productos</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

@extends('layouts.app')
@section('content')

	<div class="container">
	<h3><b> Listado de Productos </b><a class="btn btn-success float-right mb-3" style="margin:3px;" href="{{ route('home') }}">Volver</a><a class="btn btn-primary float-right mb-3" style="margin:3px;" href="{{ route('productos.create') }}">Crear producto</a>
	</h3>
	<br>
	@if(empty($productos))
		<div class="alert alert-warning">
			<h1><b>La lista de productos está vacía</b></h1>
		</div>
	@else
		<div class="class-resposive">
			<table class="table table-light table-hover">
				<thead class="">
					<tr>
						<th>Nombre Proveedor</th>
						<th>Nombre del Producto</th>
						<th>Cantidad del Producto</th>
						<th>Cantidad Límite</th>
						<th>Precio Unitario</th>
						<th>Estado</th>
						<th>Acciones</th>

					</tr>



				</thead>
				<tbody>
					@foreach($productos as $producto)
					<tr>
                        @if($producto->Estado == 1)
						<td>{{ $producto->NombreProveedor }}</td>
						<td>{{ $producto->NombreProducto}}</td>
						<td>{{ $producto->CantidadProducto }}</td>
						<td>{{ $producto->CantidadLimite }}</td>
						<td>{{ $producto->PrecioUnitario}}</td>
						<td><p>Activo</p></td>

						<td><a href="{{ route('productos.edit', ['producto' => $producto->id]) }}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></i></button></a>
							<a href="{{ route('productos.verpdf') }}"><button type="button" class="btn btn-light "><i class="fas fa-file-pdf" style="color: rgb(230, 43, 43)"></i></button></a>
							<form action="{{ route('productos.destroy', ['producto' => $producto->id]) }}" class="d-inline" method="post">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-exchange-alt"></i></button>
							</form>
						</td>



                        @endif
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
			{{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {!! $productos->links() !!}
        </div>
		</div>
	@endif

@endsection
</body>
</html>
