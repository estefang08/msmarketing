    <h1>Lista productos</h1>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<body>
    <div class="col-lg-12">
    <table class="col-lg-12">
           
<div class="container">
     <div style="background-color: #688a7e; height: 20px"></div>
    <div class="class-resposive">
        <table class="table table-striped">
        <table class="table table-striped" border="5" cellpadding="10" width="100%" style="margin-bottom: 100px;">
        <tr>
            <th class="col-lg-4" style="text-align: left" width="15%">Id Productos</th>
            <th style="text-align: center">Id Categoria</th>
            <th style="text-align: center">Nombre Proveedor</th>
            <th style="text-align: center">Nombre del Producto</th>
            <th style="text-align: center">Cantidad del Producto</th>
            <th style="text-align: center">Cantidad Limite</th>
            <th style="text-align: center">Precio Unitario</th>
        </tr>
       @foreach($productos as $producto)
        <tr>
            <td style="text-align: center">{{ $producto->id }}</td>
            <td style="text-align: center">{{ $producto->idCategoria}}</td>
            <td style="text-align: center">{{ $producto->idProveedor }}</td>
            <td style="text-align: left">{{ $producto->NombreProducto}}</td>
            <td style="text-align: center">{{ $producto->CantidadProducto }}</td>
            <td style="text-align: center">{{ $producto->CantidadLimite }}</td>
            <td style="text-align: center">{{ $producto->PrecioUnitario}}</td>
        </tr>
        

        @endforeach

        