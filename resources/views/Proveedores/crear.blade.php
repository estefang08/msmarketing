<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Crear proveedores</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h2>Crear Proveedores<a class="btn btn-primary float-right mb-3" style="margin:3px;" href="{{ route('proveedores.index') }}">Volver</a></h2>
	<form action="{{ route('proveedores.store') }}" id="FrmProveedores" method="POST">
		@csrf
		<div class="form-row">
			<label>Nombre del proveedor</label>
			<input class="form-control" type="text" name="NombreProveedor" value="{{ old('NombreProveedor') }}" required>
		</div>
		<div class="form-row">
			<label>Teléfono</label>
			<input class="form-control" type="text" maxlength="10" id="Telefono" name="Telefono" value="{{ old('Telefono') }}" required>
		</div>
		<div class="form-row">
			<label>Correo</label>
			<input class="form-control @error('email') is-invalid @enderror" type="email"  maxlength="30" id="Correo" name="Correo" value="{{ old('Correo') }}" required>
        </div>

        <div class="form-row">
			<input class="form-control" type="hidden" name="Estado" id="Estado" value="1">
		</div>


		<div class="form-row">
			<button type="submit" class="btn btn-success btn-lg mt-3">Añadir Proveedor</button>
		</div>


	</form>
</body>
</html>
@endsection
@section("script")
<script>
	/*
	$(document).ready(function() {
    $("#Telefono").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
$(document).ready(function() {
	$("#FrmProveedores").submit(function(event) {
        event.preventDefault();
        var validarcorreo = true;
		if($("#Correo").val().indexOf('@', 0) == -1 || $("#Correo").val().indexOf('.', 0) == -1) {
            alert("No es válido");
            validarcorreo = false;
        }
        if(validarcorreo = true){
        	var url = "{{ route('proveedores.index') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#FrmProveedores").serialize(),
                success: function(data) {
                    $('#RespuestaTransaccionProveedor').html(data);
                }
            });
        }else{
        	alert("Correo no permitido");
        }
	})
})
*/
	</script>
@endsection
