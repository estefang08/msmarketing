<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Modificar proveedores</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h2>Editar Proveedor<a class="btn btn-success float-right mb-3" style="margin:3px;" href="{{ route('proveedores.index') }}">Volver</a></h2>
	<form action="{{ route('proveedores.update', ['proveedor' => $proveedor->id]) }}" method="POST">
		@csrf
		@method('PUT')
		<br>
		<div class="form-row">
			<label>Nombre del proveedor</label>
			<input class="form-control" type="text" name="NombreProveedor" value="{{ $proveedor->NombreProveedor }}" required>
		</div>
		<div class="form-row">
			<label>Telefono</label>
			<input class="form-control" type="number" name="Telefono" value="{{ $proveedor->Telefono }}" required>
		</div>
		<div class="form-row">
			<label>Correo</label>
			<input class="form-control" type="text" name="Correo" value="{{ $proveedor->Correo }}" required>
		</div>
		<div class="form-row">
			<input class="form-control" type="hidden" name="Estado" id="Estado" value="{{ $proveedor->Estado }}">
		</div>

		<div class="form-row">
			<button type="submit" class="btn btn-primary btn-lg mt-3">Editar proveedor</button>
		</div>


	</form>

@endsection
</body>
</html>
