<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Proveedores</title>
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h3>Listado de Proveedores<a class="btn btn-success float-right mb-3" style="margin:3px;" href="{{ route('home') }}">Volver</a><a class="btn btn-primary float-right mb-3" style="margin:3px;" href="{{ route('proveedores.create') }}">Crear proveedor</a>
	</h3	>
	@if(empty($proveedores))
		<div class="alert alert-warning">
			La lista de proveedores está vacía
		</div>
	@else
		<div class="table table-light table-hover">
			<table class="">
				<thead class="">
					<tr>
						<th>Id </th>
						<th>Nombre del Proveedor</th>
						<th>Teléfono</th>
						<th>Correo</th>
						<th>Estado</th>
						<th>Acciones</th>
					</tr>



				</thead>
				<tbody>
					@foreach($proveedores as $proveedor)
					<tr>
						<td>{{ $proveedor->id }}</td>
						<td>{{ $proveedor->NombreProveedor }}</td>
						<td>{{ $proveedor->Telefono }}</td>
						<td>{{ $proveedor->Correo }}</td>
						@if( $proveedor->Estado == 1)
						<td><p>Activo</p></td>
						@else
						<td><p>Inactivo</p></td>
						@endif
						<td><a href="{{ route('proveedores.edit', ['proveedor' => $proveedor->id]) }}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button></a>


							<form action="{{ route('proveedores.destroy', ['proveedor' => $proveedor->id]) }}" class="d-inline" method="post">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-exchange-alt"></i></button>
							</form>
						</td>




					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		{{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {!! $proveedores->links() !!}
        </div>
	@endif
</body>
</html>
@endsection
