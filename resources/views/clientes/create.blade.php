<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Crear cliente</title>
</head>
<body>
@extends('layouts.app')

@section('content')



<form action="{{url ('/clientes')}}" method="post">
    {{csrf_field()}}


    <div class="container">
    <div class="row">
        <div class="form-group col-md-6">
            <label>Tipos de Identificación</label>
            <select name="Tipo_identificacion" class="form-control {{$errors->has('Tipo_identificacion')?'is-invalid':''}}"
                value="{{isset($user->Tipo_identificacion)?$user->Tipo_identificacion:old('Tipo_identificacion')}}">
                <option selected disabled>Tipo de Identificación</option>
                <option value="Cédula de ciudadanía">Cédula de ciudadanía</option>
                <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                <option value="Cédula de extranjería">Cédula de extranjería</option>
            </select>
            {!! $errors->first('Tipo_identificacion', '<div class="invalid-feedback">El campo Tipo identificación es obligatorio</div>')!!}
        </div>




        <div class="form-group col-md-6">
            <label>Número de identificación</label>
            <input type="number" name="id"   class="form-control {{$errors->has('id')?'is-invalid':''}}" placeholder="Número de identificación">
            {!! $errors->first('numero_identificacion', '<div class="invalid-feedback">El campo numero identificación es obligatorio</div>')!!}

        </div>










    </div>



    <div class="row">
        <div class="form-group col-md-6">
            <label>Nombre</label>
            <input type="text"  class="form-control {{$errors->has('nombre')?'is-invalid':''}}" name="nombre" placeholder="Nombre">
            {!! $errors->first('nombre', '<div class="invalid-feedback">El campo nombre es obligatorio</div>')!!}

        </div>
        <div class="form-group col-md-6">
            <label>Apellido</label>
            <input type="text" name="apellido"  class="form-control {{$errors->has('apellido')?'is-invalid':''}}" placeholder="Apellido">
            {!! $errors->first('apellido', '<div class="invalid-feedback">El campo apellido es obligatorio</div>')!!}
        </div>



        <div class="form-group col-md-6">
            <label>Edad</label>
            <input type="number"  class="form-control {{$errors->has('edad')?'is-invalid':''}}" name="edad" placeholder="Edad">
            {!! $errors->first('edad', '<div class="invalid-feedback">El campo edad es obligatorio</div>')!!}

        </div>
        <div class="form-group col-md-6">
            <label>Celular</label>
            <input type="number"  class="form-control {{$errors->has('celular')?'is-invalid':''}}" name="celular" placeholder="Celular">
            {!! $errors->first('celular', '<div class="invalid-feedback">El campo edad es obligatorio</div>')!!}

        </div>


    </div>

    <button type="submit" class="btn btn-primary">Guardar cliente</button>
    <button type="reset" class="btn btn-danger">Borrar Campos</button>
    <a href="{{url('clientes')}}" class="btn btn-primary">Regresar</a>


</form>
</div>
@endsection
</body>
</html>
