<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Modificar cliente</title>
</head>
<body>
@extends('layouts.app')

@section('content')




<div class="container">
        <div class="row">
            <div class="col-sm-6">
            <h2>Editar cliente {{$cliente->nombre}}</h2>
                @if ($errors->any())


                @endif
            </div>
        </div>

<form action="{{ url ('/clientes/' . $cliente->id) }}" method="post">
{{ csrf_field()}}
{{method_field('PATCH')}}


<div class="container">
    <div class="row">


        <div class="form-group col-md-4">
            <label>Tipo identificacioon</label>
            <select name="Tipo_identificacion" class="form-control" >
                <option selected disabled>Tipo de Identificación</option>
                <option value="Cédula de ciudadanía">Cédula de ciudadanía</option>
                <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                <option value="Cédula de extranjería">Cédula de extranjería</option>
            </select>
        </div>




        <div class="form-group col-md-6">
            <label>Numero identificacion</label>
            <input type="number" name="id"   value="{{$cliente->id}}" class="form-control {{$errors->has('id')?'is-invalid':('')}}" placeholder="Numero Identificacion">
            {!! $errors->first('numero_identificacion', '<div class="invalid-feedback">El campo numero de indentificacion es obligatorio y debe tener min 10 caracteres</div>')!!}

        </div>
    </div>



    <div class="row">
        <div class="form-group col-md-6">
            <label>Nombre</label>
            <input type="text"  value="{{$cliente->nombre}}" class="form-control {{$errors->has('nombre')?'is-invalid':('')}}" name="nombre" placeholder="nombre">
            {!! $errors->first('nombre', '<div class="invalid-feedback">El campo nombre es obligatorio</div>')!!}

        </div>
        <div class="form-group col-md-6">
            <label>Apellido</label>
            <input type="text" name="apellido"  value="{{$cliente->apellido}}" class="form-control {{$errors->has('apellido')?'is-invalid':('')}}" placeholder="apellido">
            {!! $errors->first('apellido', '<div class="invalid-feedback">El campo apellido es obligatori</div>')!!}

        </div>



        <div class="form-group col-md-6">
            <label>Edad</label>
            <input type="number"   value="{{$cliente->edad}}" class="form-control" name="edad" placeholder="edad">
        </div>
        <div class="form-group col-md-6">
            <label>Celular</label>
            <input type="number" value="{{$cliente->celular}}"  class="form-control" name="celular" placeholder="celular">
        </div>


    </div>


    <button type="submit" class="btn btn-primary">Guardar</button>
    <button type="reset" class="btn btn-danger">Borrar Campos</button>
    <a href="{{url('clientes')}}" class="btn btn-primary">Regresar</a>



</form>
</div>

@endsection
</body>
</html>
