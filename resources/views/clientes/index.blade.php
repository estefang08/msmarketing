<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Clientes</title>

</head>

<body>
@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Lista de clientes registrados <a href="clientes/create"><button type="button" class="btn btn-success float-right ">Agregar cliente</button></a></h2>


    <table class="table table-light table-hover">
        <br><br>
        <thead>
            <tr>
                <th scope="col">Tipo de identificación</th>
                <th scope="col">Número de identificación</th>
                <th scope="col">Primer nombre</th>
                <th scope="col">Primer apellido</th>
                <th scope="col">Edad</th>
                <th scope="col">Celular</th>
                <th scope="col">Opciones</th>

            </tr>
        </thead>

        <tbody>
            @foreach ($clientes as $cliente)
            <tr>
                <td>{{$cliente->Tipo_identificacion}}</td>
                <td>{{$cliente->id}}</td>
                <td>{{$cliente->nombre}}</td>
                <td>{{$cliente->apellido}}</td>
                <td>{{$cliente->edad}}</td>
                <td>{{$cliente->celular}}</td>
                <td>

                    <form action="{{route('clientes.destroy',$cliente->id)}}" method="POST">
                        <a href="{{route('clientes.edit', $cliente->id)}}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-user-edit"></i></button></a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach

        </tbody>
        </table>

</div>



@endsection
</body>
</html>
