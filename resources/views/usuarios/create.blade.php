<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Crear usuario</title>

</head>

<body>
    @extends('layouts.app')

    @section('content')


    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2>Registrar usuario</h2>

            </div>
        </div>

        <form action="{{url('/usuarios')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field()}}


            <div class="row">
                <div class="form-group col-md-6">
                    <label>Nombre</label>
                    <input type="text" name="name" class="form-control {{$errors->has('name')?'is-invalid':('')}}"  placeholder="Nombre"
                    value="{{isset($user->name)?$user->name:old('name')}}">
                    {!! $errors->first('name', '<div class="invalid-feedback">El campo nombre es obligatorio y debe tener min 6 caracteres</div>')!!}


                </div>
                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" value="{{isset($user->email)?$user->name:old('email')}}"  class="form-control {{$errors->has('email')?'is-invalid':('')}} "    name="email" placeholder="Email">
                    {!! $errors->first('email', '<div class="invalid-feedback">El campo email es obligatorio</div>')!!}

                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Contraseña</label>
                    <input type="password"   name="password"  value="{{isset($user->password)?$user->name:old('password')}}"     class="form-control {{$errors->has('password')?'is-invalid':('')}}"  placeholder="Contraseña">
                    {!! $errors->first('password', '<div class="invalid-feedback">El campo password es obligatorio debe contener al menos 6 caracteres.</div>')!!}
                </div>
                <div class="form-group col-md-6">
                    <label>Confirmar contraseña</label>
                    <input type="password" name="password_confirmation" class="form-control {{$errors->has('password_confirmation')?'is-invalid':('')}}"  placeholder="Confirmar contraseña">
                    {!! $errors->first('password_confirmation', '<div class="invalid-feedback">El campo password es obligatorio debe contener al menos 6 caracteres.</div>')!!}

                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Rol</label>
                    <select name="rol"  class="form-control {{$errors->has('rol')?'is-invalid':('')}}">
                        <option selected disabled>Elige un rol para este usuario</option>
                        @foreach($roles as $role )
                        <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('rol', '<div class="invalid-feedback">El campo rol es obligatorio</div>')!!}


                </div>
                <div class="form-group col-md-6">
                    <label> Imagen</label>
                    <input type="file"  class="form-control {{$errors->has('imagen')?'is-invalid':''}}" name="imagen" class="form-control" type="file">
                    {!! $errors->first('imagen', '<div class="invalid-feedback">El campo imagen es obligatorio</div>')!!}

                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary">Guardar usuario</button>
                    <button type="reset" class="btn btn-danger">Borrar Campos</button>
                    <a href="{{url('usuarios')}}" class="btn btn-primary">Regresar</a>
                </div>
            </div>
        </form>

    </div>



    @endsection


</body>

</html>
