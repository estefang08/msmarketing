<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Usuarios</title>

</head>

<body>
@extends('layouts.app')

@section('content')
@csrf
<div class="container">
    <h2>Lista de usuarios registardos <a href="usuarios/create"><button type="button" class="btn btn-success float-right ">Agregar usuario</button></a></h2>

    <h6>
        @if($search)
        <div class="alert alert-primary" role="alert">
            los resultados para tu busqueda '{{$search}}' son:
        </div>
        @endif
    </h6>




    <table class="table table-light table-hover">
        <thead>
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Correo electrónico</th>
                <th scope="col">Tipo de rol</th>
                <th scope="col">Imagen</th>
                <th scope="col">Opciones</th>

            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    @foreach($user->roles as $role)
                    {{$role->name}}
                    @endforeach
                </td>
                <td>
                    <img src="{{asset('imagenes/'.$user->imagen) }}" alt="{{ $user->imagen }}" height="50px" width="50px">
                </td>
                <td>

                    <form action="{{route('usuarios.destroy', $user->id)}}" method="POST">
                        <a href="{{ route('usuarios.show', $user->id)}}"><button type="button" class="btn btn-secondary  btn-sm"><i class="fas fa-eye"></i></button></a>
                        <a href="{{ route('usuarios.edit', $user->id) }}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-user-edit"></i></button></a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button>
                </td>
                </form>

            </tr>

            @endforeach

        </tbody>
    </table>
    <!-- aca estamos recoriendo los usuarios en un forma de lista por paginas -->

    {{ $users->links() }}
</div>



@endsection
</body>
</html>
